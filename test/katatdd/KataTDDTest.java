/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package katatdd;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author javi_
 */
public class KataTDDTest {
    KataTDD kata;
    /*
    public KataTDDTest() {
    }*/
        @Before
    public void setUp() {
        kata = new KataTDD();
    }
    
    @After
    public void tearDown() {
        kata = null;
    }
    @Test
    public void singleNumber(){
        String input = "1";
        int expected = 1;
        assertEquals(expected, kata.add(input));
    }
    
    @Test
    public void emptyInput(){
        String input = "";
        int expected = 0;
        assertEquals(expected, kata.add(input));
    }
    @Test
    public void firstEmpty(){
        String input = ",5";
        int expected = 5;
        assertEquals(expected, kata.add(input));
    }
    
        @Test
    public void secondEmpty(){
        String input = "1,";
        int expected = 1;
        assertEquals(expected, kata.add(input));
    }
    
    @Test
    public void threeNumbers(){
        String input = "1,2,3";
        int expected = 6;
        assertEquals(expected, kata.add(input));
    }
    
    @Test
    public void someSpaces(){
        String input = "1,,3,";
        int expected = 4;
        assertEquals(expected, kata.add(input));
    }
    
    @Test
    public void greaterThanOneThousand(){
        String input = "1,1000,15";
        int expected = 16;
        assertEquals(expected, kata.add(input));
    }
    
    @Test(expected = ArithmeticException.class)
    public void negativeNumbers(){
        String input = "-1";
        kata.add(input);
    }
    
    @Test
    public void jumplineDelimiter(){
        String input = "1\n1000\n15";
        int expected = 16;
        assertEquals(expected, kata.add(input));
    }
        @Test
    public void ampersandDelimiter(){
        String input = "1000&15";
        int expected = 15;
        assertEquals(expected, kata.add(input));
    }
    
    
}
