package katatdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Javier Santana
 */
public class KataTDD {

    public static void main(String[] args) throws Exception{
        String input = takeUserInput();
        System.out.println("El resultado de la suma es: " + add(input));
    }
    
    
    private static String takeUserInput() throws Exception {
        Scanner reader = new Scanner(System.in);
        System.out.println("Introduzca los números que quiera sumar: ");
        String input = reader.nextLine();
        if (input.length() <=0){
            throw new Exception("Introduzca los argumentos de la suma");
        }
        reader.close();
        return input;
    }
    

    public static int add(String numbers) {
        int sum = 0;
        List<Integer> numberList = extractNumbers(numbers);
        for (Integer element : numberList) {
            
            if (element < 1000){
                sum += element;
            }
        }

        return sum;
    }

    private static List<Integer> extractNumbers(String numbers) {
        
        List<Integer> result = new ArrayList<>();
        String delimiter = extractDelimiter(numbers);
        String[] aux;
        
        if (delimiter.equals("") && !numbers.equals("")){
            result.add(Integer.parseInt(numbers));
        }else{
            
            aux = numbers.split(delimiter);
            
            for (int i = 0; i < aux.length; i++) {
                if(isANumber(aux[i])){
                    
                    int number = Integer.parseInt(aux[i]);
                    
                    if (!isNegative(number))  
                        result.add(number);
                }        
            }            
        }
        return result;
    }

    private static boolean isANumber(String s) {
        try {  
            double d = Double.parseDouble(s);  
        }catch(NumberFormatException nfe){
            return false;  
        }  
        return true;  
    }

    private static boolean isNegative(int number) {
        if (number >= 0){
            return false;
        }else{
            throw new ArithmeticException("Los números negativos no están permitidos");
        }
        
    }

    private static String extractDelimiter(String aux) {
        for (int i = 0; i < aux.length(); i++) {
             
            if (!Character.isDigit(aux.charAt(i))) 
                
                if (aux.charAt(i) == '-')
                    throw new ArithmeticException("Los números negativos no están permitidos");
                else
                    return "" + aux.charAt(i);
            
        }
        return "";
    }
    
}
 